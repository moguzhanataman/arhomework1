﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InformationPanel : MonoBehaviour {

	/* 
	Don't show information panel initially. When user double taps a planet. 
	We will go into Information Panel scenario.
	*/
	public bool show = false;
	public GameObject toshow;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (show) {
			toshow.SetActive(true);
		} else {
			toshow.SetActive(false);
		}
	}

	public void changeState(){
		show = !show;
	}
}
