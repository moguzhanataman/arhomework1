﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {

	public float radius;
	public float speed;
	float timeCounter = 0;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		timeCounter += Time.deltaTime / radius * speed; // multiply all this with some speed variable (* speed);
		float x = radius * Mathf.Cos (timeCounter);
		float z = radius * Mathf.Sin (timeCounter);
		// Debug.Log (new Vector3 (x, y, z));
		transform.position = new Vector3 (x, transform.position.y, z);
	}
}
