﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonInteraction : MonoBehaviour
{

    private ScenarioController scenarioController;
    public Button singleObjectScenarioButton;
    public Button videoPlayButton;
    private bool videoAlreadyPlaying = false;

    void Awake()
    {
        scenarioController = GetComponent<ScenarioController>();
    }

    // Use this for initialization
    void Start()
    {
        singleObjectScenarioButton.onClick.AddListener(OnClick_BackToSolarSystemView);
        videoPlayButton.onClick.AddListener(OnClick_VideoView);
    }

    // Update is called once per frame
    void Update()
    {

        if (scenarioController.scenarioState != ScenarioController.AppState.SingleObject)
        {
            singleObjectScenarioButton.interactable = false;
        }
        else
        {
            singleObjectScenarioButton.interactable = true;
        }
    }

    // Takes back to solar system view
    // Disable button if it's in single object view.
    public void OnClick_BackToSolarSystemView()
    {

        switch (scenarioController.scenarioState)
        {
            // If we are in single obj scenario make button text Solar System Scenario
            case ScenarioController.AppState.SingleObject:
                singleObjectScenarioButton.GetComponentInChildren<Text>().text = "Go to Solar System Scenario";
                scenarioController.scenarioState = ScenarioController.AppState.SolarSystem;
                break;

            // If we are in solar system scenario make button text Single Object Scenario
            case ScenarioController.AppState.SolarSystem:
                singleObjectScenarioButton.GetComponentInChildren<Text>().text = "Go to Single Object Scenario";

                break;
            case ScenarioController.AppState.Video:
                break;
        }
    }

    // Toggles between video playing scenario and solar system.
    public void OnClick_VideoView()
    {
        scenarioController.toggleVideoRequest();
    }
}
