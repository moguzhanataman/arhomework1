﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/*
File: ScenarioController.cs
Author: Mehmet Oguzhan Ataman

Description:
    Controls transition between 2 scenarios:
        1. Solar system view scenario: Show whole solar system with names on it.
        2. Single cosmic object view scneario: Show a planet or sun but bigger.

    Handles touch input. When user touches on an object we do raycasting and find
    which object user pointed. When object selection happens we change scenario.
 */
public class ScenarioController : MonoBehaviour
{
    // Hold a reference to solar system. 
    // We will deactivate it when we click on particular planet.
    // We can activate it from UI actions.
    public GameObject solarSystem;
    public GameObject[] cosmicObjects;
    public Button scenarioChangeButton;
    public Text scenarioChangeButtonText;
    public GameObject videoPlayer;

    public enum AppState
    {
        None,
        SingleObject,
        SolarSystem,
        Video
    }

    // New cosmic objects information
    private string newCosmicObjName = "";
    private string lastObjName = "";
    private GameObject newCosmicObj = null;

    // Solar system or single object
    // UI can change this
    // If someone change the scenario state. We change scenarioStateChanged too.
    public AppState _scenarioState = AppState.None;
    public AppState scenarioState
    {
        get { return this._scenarioState; }
        set
        {
            this._scenarioState = value;
            this.scenarioStateChanged = true;
        }
    }

    // Scenario State
    private bool scenarioStateChanged = false;
    public bool videoAlreadyPlaying = false;


    void Awake()
    {
        scenarioChangeButton.interactable = false;
        videoPlayer.SetActive(false);
    }

    void Update()
    {
        // User clicked somewhere. Determine if it's an object.
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            ShootRay(ray);
        }

        // Handle state change
        if (scenarioStateChanged)
        {
            Debug.Log("scenario state: " + scenarioState.ToString());

            switch (scenarioState)
            {
                case AppState.SingleObject:
                    changeToSingleCosmicObjectScenario();
                    break;
                case AppState.SolarSystem:
                    changeToSolarSystemScenario();
                    break;
                case AppState.Video:
                    changeToVideoScenario();
                    break;
            }
            // Handled scenario change. Clear flag.
            scenarioStateChanged = false;
        }
    }


    void ShootRay(Ray ray)
    {
        // Disable raycasting when we are already in single object scenario
        if (scenarioState == AppState.SingleObject)
        {
            return;
        }

        RaycastHit rhit;
        GameObject gObjectHit = null;

        /*
        Found a clicked object. Deactivate solar system. 
        Create clicked objects prefab with bigger size in the middle of the ImageTracker.
        */
        if (Physics.Raycast(ray, out rhit, 1000.0f))
        {
            // Set selected objects properties
            gObjectHit = rhit.collider.gameObject;
            lastObjName = newCosmicObjName;
            newCosmicObjName = gObjectHit.name;

            // Set scenario state
            scenarioState = AppState.SingleObject;
        }
    }

    private void changeToSingleCosmicObjectScenario()
    {
        // Deactivate Solar System
        solarSystem.SetActive(false);
        scenarioChangeButton.interactable = true;
        scenarioChangeButtonText.text = "Back to Solar System View";

        // Create clicked cosmic object from our predefined GameObject array.
        var cosmicObjPrefab = findObjectByName(newCosmicObjName);
        newCosmicObj = Instantiate(cosmicObjPrefab, new Vector3(0, 20, 0), Quaternion.identity);
        newCosmicObj.transform.localScale = new Vector3(30, 30, 30);

        // Disable cosmic objects movement.
        // Careful: sun has no movement script
        var movementScript = newCosmicObj.GetComponent<Movement>();
        if (movementScript != null)
        {
            movementScript.enabled = false;
        }
    }

    private void changeToSolarSystemScenario()
    {
        if (newCosmicObj != null)
        {
            Destroy(newCosmicObj);
        }
        if (videoPlayer.activeSelf)
        {
            videoPlayer.SetActive(false);
            videoAlreadyPlaying = false;
        }

        // Activate Solar System
        solarSystem.SetActive(true);
        scenarioChangeButton.interactable = false;
        scenarioChangeButtonText.text = "Tap on Objects";
    }

    private void changeToVideoScenario()
    {
        // If already playing, close video, go to solar system scenario
        if (videoAlreadyPlaying)
        {
            Debug.Log("Already playing change state to solar system");
            videoAlreadyPlaying = false;
            videoPlayer.SetActive(false);
            scenarioState = AppState.SolarSystem;
        }
        // Close other scenarios. Run video
        else
        {
            Debug.Log("Start playing video");
            solarSystem.SetActive(false);
            if (newCosmicObj != null)
            {
                Destroy(newCosmicObj);
            }
            videoPlayer.SetActive(true);
            videoAlreadyPlaying = true;
        }
    }

    // Search CosmicObject array by object name
    private GameObject findObjectByName(string name)
    {
        foreach (GameObject item in this.cosmicObjects)
        {
            if (item.name.Equals(name))
                return item;
        }
        throw new System.Exception("Undefined object clicked");
    }

    public void toggleVideoRequest() {
        if (videoAlreadyPlaying) {
            scenarioState = AppState.SolarSystem;
        } else {
            scenarioState = AppState.Video;
        }
    }
}
